
local State = require 'state'

local StatePause = require 'common.class' (State)

function StatePause:_init(stack)
  self:super(stack)
  self.pause_time = 1
  self.elapsed = 0
  self.params = {}
end

function StatePause:enter(params)
  self.elapsed = 0
  self.params = params
  if params.time then
    self.pause_time = params.time
  end
end

function StatePause:update(dt)
  if self.elapsed < self.pause_time then
    self.elapsed = self.elapsed + dt
  else
    self.params.resume = true
    self:pop(self.params)
  end
end

return StatePause