
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local DamageIndicator = require 'view.damage_indicator'
local SpriteAtlas = require 'view.sprite_atlas'
local SpriteAnimations = require 'view.sprite_animations'
local BattleField = require 'view.battlefield'
local State = require 'state'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local ENCOUNTER = {}

local MESSAGES = {
  Fight = "%s attacked %s",
  Skill = "%s unleashed a skill",
  Item = "%s used an item",
  Run = "Ran away successfully",
  Skip = "%s did nothing. How boring",
  Miss = "%s missed attack on %s"
}

local DEATH_MESSAGES = {
  "%s died a horrible death",
  "%s was slain",
  "%s's head exploded",
  "%s has met their maker",
  "%s died a not so horrible death...",
  "%s was murdered",
  "%s will never see the sunlight again",
  "%s stopped moving... forever",
}

function EncounterState:_init(stack)
  self:super(stack)
  self.party = nil
  self.turns = nil
  self.next_turn = nil
end

function EncounterState:enter(params)
  -- params.part e params.encounter
  local atlas = SpriteAtlas()
  local animations = SpriteAnimations(atlas)
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local death_message = MessageBox(Vec(bfbox.left, bfbox.bottom + 48))
  local indicator = DamageIndicator()
  local n = 0
  local party_origin = battlefield:east_team_origin()
  self.turns = {}
  self.next_turn = 1
  self.party = params.party
  -- processa os personagens da party
  print('\ninitiatives:')
  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    character:set_type('ally')
    character:generate_initiative()
    self.turns[i] = character
    -- nao insere aliados mortos no atlas
    if character:is_alive() then
      atlas:add(character, pos, character:get_appearance())
    end
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  -- processa os personagens do encontro
  ENCOUNTER = params.encounter
  for i, character in ipairs(params.encounter) do
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    character:set_type('enemy')
    character:generate_initiative()
    self.turns[n + i] = character
    atlas:add(character, pos, character:get_appearance())
  end
  table.sort(self.turns, self.compare)
  self:view():add('atlas', atlas)
  self:view():add('animations', animations)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  self:view():add('death_message', death_message)
  self:view():add('damage_indicator', indicator)
  message:set("You stumble upon an encounter")
end

function EncounterState.compare(char_a, char_b)
  return char_a:get_initiative() > char_b:get_initiative()
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('animations')
  self:view():remove('battlefield')
  self:view():remove('message')
  self:view():remove('death_message')
  self:view():remove('damage_indicator')
end

function EncounterState:update(_)
  local current_character = self.turns[self.next_turn]
  self.next_turn = self.next_turn % #self.turns + 1
  -- personagens mortos não jogam
  while not current_character:is_alive() do
    current_character = self.turns[self.next_turn]
    self.next_turn = self.next_turn % #self.turns + 1
  end
  local params = { current_character = current_character }
  local next_state
  if current_character:get_type() == 'ally' then
    params.encounter = ENCOUNTER
    params.party = self.party
    next_state = 'player_turn'
  elseif current_character:get_type() == 'enemy' then
    params.encounter = self.party
    params.party = ENCOUNTER
    next_state = 'enemy_turn'
  end
  return self:push(next_state, params)
end

function EncounterState:resume(params)
  self:view():get('death_message'):set(nil)
  local message, death_message
  local option = params.action
  if option == 'Fight' then
    if not params.damage then -- errou o golpe, muda a mensagem
      option = 'Miss'
    end
    self:display_number(params, 'damage')
    message = MESSAGES[option]:format(params.character:get_name(),
    params.target:get_name())
    -- mostra mensagem de morte
    if not params.target:is_alive() then
      death_message = DEATH_MESSAGES[math.random(#DEATH_MESSAGES)]
      death_message = death_message:format(params.target:get_name())
      self:view():get('death_message'):set(death_message)
    end
  elseif option == 'Skill' then
    if params.skill.atk then
      self:display_number(params, 'damage')
    end
    if params.skill.heal or params.skill.eff == 'drain' then
      self:display_number(params, 'heal')
    end
    message = params.message:format(params.character:get_name(),
    params.target:get_name())
  elseif option ~= 'Run' then
    message = MESSAGES[option]:format(params.character:get_name())
  else
    message = MESSAGES[option]
  end
  self:view():get('message'):set(message)

  -- termina o encontro atual
  if self.check_end_condition(params.enemies) or params.action == 'Run' then
    -- se nao tiver acabado de voltar de uma pausa, entra na pausa
    if not params.resume then
      return self:push('state_pause', params)
    end
    -- caso contrario, sai do encontro
    local ret_params = { game_over = self:check_game_over() }
    return self:pop(ret_params)
  end
end

function EncounterState:display_number(params, type)
  local atlas = self:view():get('atlas')
  local target_sprite
  if type == 'damage' then
    target_sprite = atlas:get(params.target)
  elseif type == 'heal' then
    target_sprite = atlas:get(params.healed_char)
  else
    error('Invalid type for number display')
  end
  if target_sprite then
    self:view():get('damage_indicator'):show(
      target_sprite,
      params.damage,
      params.crit,
      params.heal,
      type
    )
  end
end

function EncounterState:check_game_over()
  for _, ally in ipairs(self.party) do
    if ally:is_alive() then
      return false
    end
  end
  return true
end

function EncounterState.check_end_condition(enemies)
  if enemies == nil then return false end
  for _, enemy in ipairs(enemies) do
    if enemy:is_alive() then
      return false
    end
  end
  return true
end

return EncounterState

