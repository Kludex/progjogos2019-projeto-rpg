
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'


local EnemyTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Next', 'Skip' }

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.enemies = {}
  self.cursor = nil
  self.menu = ListMenu(TURN_OPTIONS)
  self.atlas = nil
  self.animations = nil
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.enemies = params.encounter
  self.atlas = self:view():get('atlas')
  self.animations = self:view():get('animations')
  self:_show_menu()
  self:_show_cursor()
  self:_show_stats()
end

function EnemyTurnState:_show_menu()
  local bfbox = self:view():get('battlefield').bounds
  self.menu:reset_cursor()
  self.menu.position:set(bfbox.right + 32, (bfbox.top + bfbox.bottom) / 2)
  self:view():add('turn_menu', self.menu)
end

function EnemyTurnState:_show_cursor()
  local sprite_instance = self.atlas:get(self.character)
  self.cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', self.cursor)
end

function EnemyTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function EnemyTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

function EnemyTurnState:enemy_action(targets)
  -- ataca um alvo aleatorio
  local r = math.random(1, #targets)
  local target = targets[r]
  while not target:is_alive() do
    r = math.random(1, #targets)
    target = targets[r]
  end
  local result = self.character:attack(target)
  if result.damage then
    self.animations:take_damage(target)
  end
  return self:pop({
    action = 'Fight',
    character = self.character,
    target = target,
    enemies = self.enemies,
    damage = result.damage,
    hit = result.hit,
    crit = result.crit,
  })
end

function EnemyTurnState:on_keypressed(key)
  local option = TURN_OPTIONS[self.menu:current_option()]
  if key == 'down' then
    self.menu:next()
  elseif key == 'up' then
    self.menu:previous()
  elseif key == 'return' then
    -- entra em modo de ataque wooooo
    if option == 'Next' then
      self:enemy_action(self.enemies)
    else
      return self:pop({ action = option, character = self.character })
    end
  end
end

function EnemyTurnState.resume(params)
  if params.cancel then
    return
  end
end

return EnemyTurnState
