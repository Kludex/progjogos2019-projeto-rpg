
local Tween = require 'common.tween'
local Vec = require 'common.vec'

local SpriteAnimations = require 'common.class' ()

function SpriteAnimations:_init(atlas)
  self.tweens = {}
  self.return_tweens = {}
  self.atlas = atlas
  self.origins = {}
end

function SpriteAnimations:return_to_origin(instance)
  local sprite_instance = self.atlas:get(instance)
  self.return_tweens[instance] = Tween.new(0.2, sprite_instance,
  {position = self.origins[instance]}, 'outCubic')
end

function SpriteAnimations:save_origin(instance)
  if not self.origins[instance] then
    local sprite_instance = self.atlas:get(instance)
    self.origins[instance] = Vec(sprite_instance.position:get())
  end
end

function SpriteAnimations:take_damage(instance)
  local sprite_instance = self.atlas:get(instance)
  local dest_pos
  -- guarda a posição original da sprite
  self:save_origin(instance)
  if instance:get_type() == 'ally' then
    dest_pos = self.origins[instance].x + 10
  else
    dest_pos = self.origins[instance].x - 10
  end
  self.tweens[instance] = Tween.new(0.4,
  sprite_instance.position, {x = dest_pos}, 'outElastic')
end

function SpriteAnimations:attack(instance)
  local sprite_instance = self.atlas:get(instance)
  local dest_pos
  -- guarda a posição original da sprite
  self:save_origin(instance)
  if instance:get_type() == 'ally' then
    dest_pos = self.origins[instance].x - 20
  else
    dest_pos = self.origins[instance].x + 20
  end
  self.tweens[instance] = Tween.new(0.05,
  sprite_instance.position, {x = dest_pos}, 'linear')
end

function SpriteAnimations:update(dt)
  for instance, tween in pairs(self.tweens) do
    if tween:update(dt) then
      if instance:is_alive() then
        self:return_to_origin(instance)
        self.tweens[instance] = nil
      else
        -- se o personagem estiver morrido, nao roda o return e
        -- remove ele do atlas
        self.atlas:remove(instance)
      end
    end
  end
  -- roda os tweens de retorno à posição inicial
  for instance, return_tween in pairs(self.return_tweens) do
    if return_tween:update(dt) then
      self.return_tweens[instance] = nil
    end
  end
end

function SpriteAnimations.draw()
end

return SpriteAnimations