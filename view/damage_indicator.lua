
local Vec = require 'common.vec'
local Tween = require 'common.tween'
local DamageIndicator = require 'common.class' ()

function DamageIndicator:_init()
  self.tweens = {}
  self.font = love.graphics.newFont('assets/fonts/VT323-Regular.ttf', 36)
  self.font:setFilter('nearest', 'nearest')
end

function DamageIndicator:show(target_sprite, damage, crit, heal, type)
  local display
  if type == 'heal' then
    self.heal_offset = Vec(-24, -48)
    self.heal_alpha = 1
    display = tostring(heal)
    self.tweens.heal_offset = Tween.new(0.5, self,
    {heal_offset = Vec(-24, -58)}, 'outQuad')
    self.tweens.heal_alpha = Tween.new(0.5, self,
    {heal_alpha = 0}, 'inQuad')
    self.heal_display = love.graphics.newText(self.font, display)
    self.heal_target_pos = Vec(target_sprite.position:get())
  elseif type == 'damage' then
    self.dam_offset = Vec(-24, -48)
    self.dam_alpha = 1
    if damage then
      display = tostring(damage)
      if crit then
        display = display .. '!'
      end
    else
      display = 'miss'
    end
    self.tweens.dam_offset = Tween.new(0.5, self,
    {dam_offset = Vec(-24, -58)}, 'outQuad')
    self.tweens.dam_alpha = Tween.new(0.5, self,
    {dam_alpha = 0}, 'inQuad')
    self.dam_display = love.graphics.newText(self.font, display)
    self.dam_target_pos = Vec(target_sprite.position:get())
  end
end

function DamageIndicator:draw()
  local g = love.graphics
  local position
  if self.dam_display then
    position = self.dam_target_pos + self.dam_offset
    g.push()
    g.setColor( 1, .4, .4, self.dam_alpha)
    g.translate(position:get())
    g.draw(self.dam_display)
    g.pop()
  end
  if self.heal_display then
    position = self.heal_target_pos + self.heal_offset
    g.push()
    g.setColor( .4, 1, .4, self.heal_alpha)
    g.translate(position:get())
    g.draw(self.heal_display)
    g.pop()
  end
end

function DamageIndicator:update(dt)
  for _, tween in pairs(self.tweens) do
    tween:update(dt)
  end
end

return DamageIndicator

