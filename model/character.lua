
local Character = require 'common.class' ()

function Character:_init(spec)
  self.spec = spec
  self.hp = spec.max_hp
  self.mp = spec.max_mp
  self.atk = spec.atk
  self.spd = spec.spd
  self.def = spec.def
  self.ac = spec.ac
  self.skills = spec.skills
  self.initiative = nil
  self.type = nil
  self.alive = true
end

function Character:get_name()
  return self.spec.name
end

function Character:set_type(type)
  self.type = type
end

function Character:get_type()
  return self.type
end

function Character:get_atk()
  return self.spec.atk
end

function Character:get_spd()
  return self.spec.spd
end

function Character:get_def()
  return self.spec.def
end

function Character:get_ac()
  return self.spec.ac
end

function Character:get_skills()
  return self.spec.skills or {}
end

function Character:get_skill_names()
  local names = {}
  for i, skill in pairs(self:get_skills()) do
    local S = require('database.skills.' .. skill)
    names[i] = S.name
  end
  return names
end

function Character:generate_initiative()
  self.initiative = math.random(20) + self.spd
  print(self:get_name(), self.initiative)
end

function Character:get_initiative()
  return self.initiative
end

function Character:get_appearance()
  return self.spec.appearance
end

function Character:get_hp()
  return self.hp, self.spec.max_hp
end

function Character:get_mp()
  return self.mp, self.spec.max_mp
end

function Character:is_alive()
  return self.alive
end

function Character:heal(heal)
  local hp, max_hp = self:get_hp()
  self.hp = math.min(max_hp, hp + heal)
  return heal
end

function Character:freeze(turns)
  print(self:get_name() .. ' froze for ' .. turns .. 'turns!')
end

function Character:attack(target, atk)
  if not atk then
    atk = self.atk
  end
  local ret = {damage = nil, crit = false}
  local hit = math.random(20)
  ret.crit = hit == 20
  hit = hit + self.spd
  if hit > target:get_ac() or ret.crit then
    -- acertou
    ret.damage = target:take_damage(atk, target:get_def(), ret.crit)
  end
  return ret
end

function Character:take_damage(atk, target_def, crit)
  local damage = math.max(1, math.floor(atk*(20/(20+target_def))))
  if crit then
    damage = damage*2
  end
  self.hp = math.max(self.hp - damage, 0)
  if self.hp == 0 and self.alive then
    self:die()
  end
  return damage
end

function Character:use_skill(skill, target)
  local result = {}
  if skill.atk then
    result = self:attack(target, skill.atk)
  elseif skill.heal then
    result.heal = target:heal(skill.heal)
    result.healed_char = target
  end
  if skill.eff == 'drain' then
    result = self:attack(target, skill.atk)
    result.heal = self:heal(skill.eff_info)
    result.healed_char = self
  elseif skill.eff == 'freeze' then
    target:freeze(skill.eff_info)
  end
  self.mp = self.mp - skill.cost
  return result
end

function Character:die()
  self.alive = false
  print(self:get_name() .. " died!")
end

return Character

