
return {
  name = "Greater Heal",
  cost = 5,
  heal = 8,
  message = "%s healed %s!"
}