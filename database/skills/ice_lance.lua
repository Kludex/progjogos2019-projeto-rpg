
return {
  name = "Ice Lance",
  cost = 5,
  atk = 8,
  eff = 'freeze', -- effect
  eff_info = 1, -- freeze for 1 turn
  message = "%s unleashed an Ice Lance on %s!"
}