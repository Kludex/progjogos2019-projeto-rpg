
return {
  name = "Lesser Heal",
  cost = 3,
  heal = 4,
  message = "%s healed %s!"
}