
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 16,
  max_mp = 10,
  atk = 8,
  spd = 8,
  def = 8,
  ac = 16, -- armor class, define a esquiva
}

