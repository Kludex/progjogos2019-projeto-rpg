
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 8,
  max_mp = 20,
  atk = 2,
  spd = 6,
  def = 2,
  ac = 10, -- armor class, define a esquiva
  skills = {
    'greater_heal',
    'lesser_heal',
    'life_drain'
  }
}

