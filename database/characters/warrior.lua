
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  max_mp = 12,
  atk = 6,
  spd = 10,
  def = 6,
  ac = 16, -- armor class, define a esquiva
  skills = {
    'ice_lance'
  }
}

