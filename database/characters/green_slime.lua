
return {
  name = "Green Slime",
  appearance = 'slime',
  max_hp = 6,
  max_mp = 5,
  atk = 2,
  spd = 4,
  def = 2,
  ac = 7, -- armor class, define a esquiva
}

