
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  max_mp = 10,
  atk = 5,
  spd = 9,
  def = 4,
  ac = 13, -- armor class, define a esquiva
  skills = {
    'lesser_heal'
  }
}

